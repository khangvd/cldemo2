#!/bin/bash

set -x
set -e

# transfers images in parallel 
python3 ./ci-common/image-transfer.py
echo "Image transfer done"

#delete the images in the folder
echo "Cleanup the image build directory"
#go find the image_dest_folder from snapshots.sh
image_dest_folder=`cat ci-common/snapshots.sh | grep ^image_dest_folder= | cut -d'=' -f2 | tr -d '"'`

if [ -z "$image_dest_folder" ]
then
  echo "Fail: Couldn't detect where the source image folder is"
  exit 1
fi

rm ${image_dest_folder}/*.qcow2
